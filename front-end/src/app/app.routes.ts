import {Routes} from "@angular/router";
import {AppComponent} from "./app.component";
import {ProjectRoutes} from "./project/project.route";
import {ProjectsListComponent} from "./project/ui/projects-list.component";
import {CreateProjectComponent} from "./project/ui/create-project.component";
import {ProjectDetailComponent} from "./project/ui/project-detail.component";
import { ProjectRowDataComponent } from "./project/project-row-data/project-row-data.component";
import { VisualizeResultComponent } from "./project/visualize-result/visualize-result.component";
 
export const APP_ROUTES: Routes = [
  {
    path: '', component:ProjectsListComponent
  },
  {
    path: 'projects',
    children: [
      {path: '', component: ProjectsListComponent},
      {path: 'create', component: CreateProjectComponent},
      {path: ':id', component: ProjectDetailComponent},
    ]
  }
  // { path: '**',    component: _404PageComponent },
];
