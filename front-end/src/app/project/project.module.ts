import {NgModule} from "@angular/core/src/metadata/ng_module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {RouterModule} from "@angular/router";

import {ToastModule, ToastsManager} from 'ng2-toastr/ng2-toastr';
import { MaterialModule } from '@angular/material';
import {NgUploaderModule} from "ngx-uploader";
// import {Md2AccordionModule} from 'md2-accordion/accordion';

import {CreateProjectComponent} from "./ui/create-project.component";
import {ProjectDetailComponent} from "./ui/project-detail.component";
import {ProjectsListComponent} from "./ui/projects-list.component";
import {ProjectService} from "./services/project.service";
import { SideBarComponent } from "./side-bar/side-bar.component";
import { ProjectRowDataComponent } from "./project-row-data/project-row-data.component";
import { VisualizeResultComponent } from "./visualize-result/visualize-result.component";
import { ScrollableTableModule } from '../shared/scrollable-table/scrollable-table.module';

@NgModule({
  declarations: [
    CreateProjectComponent,
    ProjectDetailComponent,
    ProjectsListComponent,
    SideBarComponent,
    VisualizeResultComponent,
    ProjectRowDataComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserModule,
    RouterModule,
    ReactiveFormsModule,
    MaterialModule.forRoot(),
    ToastModule,
    NgUploaderModule,
    ScrollableTableModule
    // Md2AccordionModule
  ],
  providers: [ProjectService, ToastsManager],
  exports: [ProjectsListComponent, ProjectDetailComponent, CreateProjectComponent]
})
export class ProjectModule { }
