import {Routes} from "@angular/router";

import {ProjectDetailComponent} from "./ui/project-detail.component";
import {CreateProjectComponent} from "./ui/create-project.component";
import {ProjectsListComponent} from "./ui/projects-list.component";
import { ProjectRowDataComponent } from "./project-row-data/project-row-data.component";

export const ProjectRoutes: Routes = [

  {
    path: 'projects',
    children: [
      {path: '', component: ProjectsListComponent},
      {path: 'create]', component: CreateProjectComponent},
      {path: ':id', component: ProjectDetailComponent}
    ]
  }

];
