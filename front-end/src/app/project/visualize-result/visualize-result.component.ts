import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'visualize-result',
  templateUrl: './visualize-result.component.html',
  styleUrls: ['./visualize-result.component.css']
})
export class VisualizeResultComponent implements OnInit {

  @Input() result: string = "Visualizer goes here.";
  constructor() { }

  ngOnInit() {
  }


}

